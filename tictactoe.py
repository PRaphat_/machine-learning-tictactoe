"""The main (startup) function for the application."""

from board import TicTacToeBoard
tttboard = TicTacToeBoard()
def game():
    tttboard.drawBoard()
    while(tttboard.end == False):
        print("It's your turn, " + tttboard.turn)
        move = int(input("Please select the position to mark(1-9) :"))        
        if tttboard.board[move - 1] == '.':
            tttboard.board[move - 1] = tttboard.turn
            tttboard.mark(tttboard.turn,int(move))
        else:
            print("That place is already filled.\nMove to which place?")
            tttboard.drawBoard()
            continue
    if tttboard.end == True and tttboard.draw == False:
        tttboard.drawBoard()
        print("Winner is "+ tttboard.turn )
    elif tttboard.end == True and tttboard.draw == True:
        tttboard.drawBoard()
        print("DRAW, Game Over")

#------------------------------------------------------------------------------
if __name__ == "__main__":
    game()